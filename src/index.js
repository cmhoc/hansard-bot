import config from '../config.js';
import { Client, WebhookClient } from 'discord.js';

import {
    getUserFromDiscordId, getLocationFromChannel, getCharacterForLocation,
    getCharacterParty, getCharacterCabinetRoles, getCharacterSeat
} from './services/db.js';

const client  = new Client();

client.once('ready', () => {
    console.log('Ready!');
});

client.on('message', async (message) => {
    if (!message.author.bot) {
        try {
            const location = await getLocationFromChannel(message.channel.id);
            if (location === undefined) {
                return false;
            }

            const webhook = new WebhookClient(location.webhook_id, location.webhook_token);

            message.delete();

            const user = await getUserFromDiscordId(message.author.id);
            if (user === undefined) {
                message.author.send("You can't debate until you've made a character, contact Clerk Bot or a member of the moderation team to get setup");
                return false;
            }

            const char = await getCharacterForLocation(user.id, location.id);
            if (char === undefined) {
                message.author.send("You can't debate until you've made a character, contact Clerk Bot or a member of the moderation team to get setup");
                return false;
            }

            const party    = await getCharacterParty(char.id);
            const cabinet  = await getCharacterCabinetRoles(char.id);
            const currSeat = await getCharacterSeat(char.id);

            const prefix = setPrefix(cabinet, location.provincial);
            const suffix = setSuffix(prefix, location, currSeat);

            const name = prefix + char.char_name + ' ' + suffix + '(' + (party === undefined ? 'IND' : party.abbr) + ')';

            webhook.send(`Mr. Speaker,\r\n${message.content}`, {
                "username" : name,
                "avatarURL" : message.author.displayAvatarURL()
            });
        } catch (error) {
            throw error;
        }
    }
});

function setPrefix(cabinet, location) {
    let prefix = '';

    cabinet.forEach(function(role) {
        if (role.role === 'Prime Minister') { // Current and former PMs
            prefix = 'Rt. Hon. ';
        } else if (prefix !== 'Rt. Hon.' && location === 0) { // Current and former federal cabinet ministers
            prefix = 'Hon. ';
        } else if (prefix !== 'Rt. Hon.' && location === 1 && date_left === null) { // Current provincial cabinet ministers
            prefix = 'Hon. ';
        }
    })

    return prefix;
}

function setSuffix(prefix, location, parliament_seat) {
    let suffix = '';

    if (parliament_seat !== undefined) {
        suffix = location.member_suffix + ' ';
    }

    if (location.provincial === 0 && prefix !== '') {
        suffix += 'PC ';
    }

    return suffix;
}

client.login(config.token);