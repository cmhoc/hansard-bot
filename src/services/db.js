import mysql from 'mysql2/promise';
import config from '../../config.js';

const pool = mysql.createPool({
  host     : config.db.host,
  port     : config.db.port,
  database : config.db.database,
  user     : config.db.user,
  password : config.db.password
});

export async function getUserFromDiscordId (discord_id) {
  const connection = await pool.getConnection();

  try {
    const [rows, fields] = await connection.execute(`
    SELECT * 
    FROM users 
    WHERE discord_id = ?`, [discord_id]);

    return rows[0];
  } catch (error) {
    throw error;
  } finally {
    connection.release();
  }
}

export async function getLocationFromChannel (channel_id) {
  const connection = await pool.getConnection();

  try {
    const [rows, fields] = await connection.execute(`
    SELECT * 
    FROM locations 
    WHERE debate_channel = ?`, [channel_id]);

    return rows[0];
  } catch (error) {
    throw error;
  } finally {
    connection.release();
  }
}

export async function getCharacterForLocation (user_id, location_id) {
  const connection = await pool.getConnection();

  try {
    const [rows, fields] = await connection.execute(`
    SELECT * 
    FROM characters 
    WHERE char_loc = ? 
    AND user_id = ?`, [location_id, user_id]);

    return rows[0];
  } catch (error) {
    throw error;
  } finally {
    connection.release();
  }
}

export async function getCharacterParty (char_id) {
  const connection = await pool.getConnection();

  try {
    const [rows, fields] = await connection.execute(`
      SELECT *, parties.id as party_id
      FROM party_membership, parties 
      WHERE char_id = ? 
      AND date_left IS NULL
      AND party_id = parties.id`, [char_id]);

    return rows[0];
  } catch (error) {
    throw error;
  } finally {
    connection.release();
  }
}

export async function getCharacterSeat (char_id) {
  const connection = await pool.getConnection();

  try {
    const [rows, fields] = await connection.execute(`
      SELECT *
      FROM seat_history 
      WHERE char_id = ? 
      AND date_left IS NULL`, [char_id]);

    return rows[0];
  } catch (error) {
    throw error;
  } finally {
    connection.release();
  }
}

export async function getCharacterCabinetRoles (char_id) {
  const connection = await pool.getConnection();

  try {
    const [rows, fields] = await connection.execute(`
      SELECT *, cabinet.id as cabinet_id
      FROM cabinet_history, cabinet 
      WHERE char_id = ?
      AND cabinet_role_id = cabinet.id`, [char_id]);

    return rows;
  } catch (error) {
    throw error;
  } finally {
    connection.release();
  }
}